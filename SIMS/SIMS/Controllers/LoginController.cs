﻿
using BusinessLayer;
using BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace SIMS.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Login/
        UserManager objUserManager = new UserManager();
        User objUser;
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoginHome(FormCollection form)
        {
            objUser = objUserManager.GetUser(form["user_Name"].ToString());
            if (form != null)
            {
                string UserName = form["user_Name"].ToString();
                string Password = form["_password"].ToString();
                if (UserName == "dinesh" && Password == "test")
                {
                    
                    FormsAuthentication.SetAuthCookie(UserName, false);
                    TempData["UserCallingName"] = "Dinesh Subasingha";
                    return RedirectToAction("Home", "Home");
                }
            }
            TempData["message"] = "User Name or Password Incorrect..";
            return RedirectToAction ("Login","Login");
        }
	}
}