﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace SIMS.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        
        public ActionResult Home()
        {
            return View();
        }

        
    }
}