﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Execute
{
    public class clsExecute<T>
    {


        static SqlCommand com = clsConnection.GetSqlCommand();
        static SqlConnection con = clsConnection.OpenConnection();

        public static T Execute(string Query, params SqlParameter[] para)
        {


            return xxx(Query, CommandType.StoredProcedure, para);

        }
        public static T Execute(string Query, CommandType cmd, params SqlParameter[] para)
        {


            return xxx(Query, cmd, para);

        }

        public static T ExecuteNonQuery(string Query, CommandType cmd, params SqlParameter[] para)
        {
            return NonQuery(Query, cmd, para);

        }

        private static T NonQuery(string query, CommandType cmdType, params SqlParameter[] para)
        {




            //SqlCommand com;
            //com = new SqlCommand(query, con);
            com.Connection = con;
            com.CommandText = query;
            com.CommandType = cmdType;
            com.Parameters.Clear();
            com.Parameters.AddRange(para);

            SqlDataAdapter da = new SqlDataAdapter(com);

            if (typeof(T) == typeof(int) || typeof(T) == typeof(decimal) || typeof(T) == typeof(DateTime))
            {
                return (T)Convert.ChangeType(com.ExecuteNonQuery(), typeof(T));

            }
            else
            {
                throw new Exception("Invalid type");
            }

        }
        private static T xxx(string query, CommandType cmdType, params SqlParameter[] para)
        {

            DataTable dt;
            DataSet ds;
            //SqlConnection con = clsConnection.OpenConnection();
            //SqlCommand com;
            //com = new SqlCommand(query, con);
            com.Connection = con;
            com.CommandText = query;
            com.CommandType = cmdType;
            com.Parameters.Clear();
            com.Parameters.AddRange(para);

            SqlDataAdapter da = new SqlDataAdapter(com);
            if (typeof(T) == typeof(DataTable))
            {
                dt = new DataTable();
                da.Fill(dt);
                para = null;
                return (T)Convert.ChangeType(dt, typeof(T));
            }
            else if (typeof(T) == typeof(DataRow))
            {
                dt = new DataTable();
                da.Fill(dt); para = null;
                return (T)Convert.ChangeType(dt.Rows[0], typeof(T));
            }
            else if (typeof(T) == typeof(DataSet))
            {
                ds = new DataSet();
                da.Fill(ds); para = null;
                return (T)Convert.ChangeType(ds, typeof(T));
            }
            else if (typeof(T) == typeof(int) || typeof(T) == typeof(decimal) || typeof(T) == typeof(DateTime))
            {
                dt = new DataTable();
                da.Fill(dt); para = null;
                if (dt.Rows.Count == 0)
                {
                    return (T)Convert.ChangeType(default(T), typeof(T));
                }
                else
                {

                    return (T)Convert.ChangeType(dt.Rows[0][0], typeof(T));
                }
            }
            else
            {
                throw new Exception("Invalid type");
            }

        }
        public static void BiginTrans()
        {
            com.Transaction = con.BeginTransaction();
        }

        public static void ComitTrans()
        {
            com.Transaction.Commit();
        }

        public static void RollbackTrans()
        {
            com.Transaction.Rollback();
        }

    }

    #region Param
    public class param
    {
        public static SqlParameter Add(string ParamName, object value)
        {
            return new SqlParameter(ParamName, value);
        }
    }
    #endregion
}
