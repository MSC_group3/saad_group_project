﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DataAccessLayer.Execute
{
    public class clsConnection
    {
        private static SqlConnection con;
        private static SqlCommand cmd;
        public static SqlConnection OpenConnection()
        {
            if (con == null)
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);

            }
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            //else
            //{
            //    con.Close();
            //}
            return con;
        }

        public static SqlCommand GetSqlCommand()
        {
            if (cmd == null)
            {
                cmd = new SqlCommand();
            }
            return cmd;

        }
    }
}
