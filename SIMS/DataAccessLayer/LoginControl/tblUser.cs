﻿
using BusinessObject;
using DataAccessLayer.Execute;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace DataAccessLayer
{
public class tblUser
    {
        public User GetUser(string UserName)
        {
            User objUser = new User();
            DataRow dr = clsExecute<DataRow>.Execute(@"SELECT [UserID]
                                                      ,[UserName]
                                                      ,[Password]
                                                      ,[RoleID]
                                             FROM [StudentDB].[dbo].[tblUser]", CommandType.Text);
            if (dr != null)
            {
                objUser = new User
                {
                    UserID = Convert.ToInt32(dr["UserID"]),
                    UserName = dr["UserName"].ToString(),
                };
                return objUser;
            }
            return objUser;
        }
    }
}
